package program1;

import java.util.Comparator;

public class CustomComparator implements Comparator<Task> {
    @Override
    public int compare(Task o1, Task o2) {
    	if(o1.getDate() != null && o2.getDate() != null)
    		return o1.getDate().compareTo(o2.getDate());
    	else
    		return 0;
    }
}