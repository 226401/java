package program1;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


public class Project extends Obiekty implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private static final AtomicInteger count = new AtomicInteger(0);
	private final int projectID;
	private String nazwa = null;
	private List<Person> personList = new ArrayList<Person>();
	private List<Task> taskList = new ArrayList<Task>();
	
	public Project() {
		projectID = count.getAndIncrement(); 
	}
	
	public Project( String b) {
		nazwa = b;
		projectID = count.getAndIncrement(); 
	}
	
	public void show(){
		System.out.println("nazwa =  "+ nazwa + "; id = " + projectID);
		if(!personList.isEmpty()) {
			for(Person osoba : personList) {
				String tmp_name = osoba.getName();
				int tmp_id = osoba.getID();
				System.out.println("Czescia projektu jest " + tmp_name + " o ID " + tmp_id);
			}
		}
		if(!taskList.isEmpty()) {
			for(Task zadanie : taskList) {
				String tmp_name = zadanie.getText();
				int tmp_id = zadanie.getID();
				System.out.println("Czescia projektu jest zadanie " + tmp_name + " o ID " + tmp_id);
			}
		}
	}
	
	public void addPerson(Person e) {
		personList.add(e);
	}
	
	public void addTask(Task e) {
		taskList.add(e);
	}
	
	public String getName() {
		return nazwa;
	}
	
	public void removeTask(int e) {
		taskList.remove(e);
	}
	
	public void removePerson(int e) {
		personList.remove(e);
	}
	
	public void setName(String nazwaa) {
		nazwa = nazwaa;
	}
	
	public int getID() {
		return projectID;
	}
	
	public Task getTask(int id) {
		return taskList.get(id);
	}
	
	public Person getPerson(int id) {
		return personList.get(id);
	}
	
	@Override
	public boolean equals(Object o) 
	{
	    if (o instanceof Project) 
	    {
	    	Project c = (Project) o;
	      if (nazwa.equals(c.nazwa) ) //whatever here
	         return true;
	    }
	    return false;
	}
	
	@Override
	public String toString() {
		return "Project [projectID=" + projectID + ", nazwa=" + nazwa + ", personList=" + personList + ", taskList="
				+ taskList + "]";
	}
}