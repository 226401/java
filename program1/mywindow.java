package program1;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.io.*;



//usu� osob� 




public class mywindow {
	/**
	 * @param args
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		//deklaracje
		Scanner inputReader = new Scanner(System.in);
		String input = null;
		List<Project> projekty = new ArrayList<Project>();
		List<Person> osoby = new ArrayList<Person>();
		List<Task> zadania = new ArrayList<Task>();
		
		//nieskonczona petla w ktorej jest caly program
		while(true)
		{
			//wczytuje polecenie
			System.out.print("> ");
			input = inputReader.nextLine();
			
			//sprawdzam czy nalezy zakonczyc program
			if(input.trim().equals("quit")){
				break;
			}
			
			//dziele string
			String[] splitStrings = input.split(" ");
			String polecenie = splitStrings[0];
			
			//sprawdzam co uzytkownik chce zrobic
			switch(polecenie){
				case "add":
					if (splitStrings[1].equals("person")) {
						osoby.add(new Person(splitStrings[2]));
						System.out.println("Dodalem osobe");
						break;
					}
					if (splitStrings[1].equals("project")) {
						projekty.add(new Project(splitStrings[2]));
						System.out.println("Dodalem projekt");
						break;
					}
					if (splitStrings[1].equals("person2project")) {
						int nr = Integer.parseInt(splitStrings[2]);
						int nr2 = Integer.parseInt(splitStrings[3]);
						projekty.get(nr).addPerson(osoby.get(nr2));
						System.out.println("Dodalem osobe " + osoby.get(nr2).getName() + " do projektu " + projekty.get(nr).getName());
						break;
					}
					if (splitStrings[1].equals("task")) {	
						if(splitStrings.length>3) {
							zadania.add(new Task(splitStrings[2],splitStrings[3]));
						}else {
							zadania.add(new Task(splitStrings[2]));
						}
						System.out.println("Dodalem zadanie");
						break;
					}
					if (splitStrings[1].equals("task2project")) {
						int nr = Integer.parseInt(splitStrings[2]);
						int nr2 = Integer.parseInt(splitStrings[3]);
						projekty.get(nr).addTask(zadania.get(nr2));
						System.out.println("Dodalem zadanie " + zadania.get(nr2).getText() + " do projektu " + projekty.get(nr).getName());
						break;
					}
					System.out.println("Bledne polecenie");
					break;
					
				case "show":
					if(splitStrings[1].equals("project")) {
						int nr = Integer.parseInt(splitStrings[2]);
						projekty.get(nr).show();
						break;
					}
					if(splitStrings[1].equals("person")) {
						int nr = Integer.parseInt(splitStrings[2]);
						osoby.get(nr).show();
						break;
					}
					if(splitStrings[1].equals("task")) {
						int nr = Integer.parseInt(splitStrings[2]);
						zadania.get(nr).show();
						break;
					}
					System.out.println("Bledne polecenie");
					break;
					
				case "remove":
					if(splitStrings[1].equals("project")) {
						int nr = Integer.parseInt(splitStrings[2]);
						projekty.remove(nr);
						System.out.println("Usunalem projekt");
						break;	
					}
					if(splitStrings[1].equals("person")) {
						int nr = Integer.parseInt(splitStrings[2]);
						String imie = osoby.get(nr).getName();
						osoby.remove(nr);
						System.out.println("Usunalem osobe o imieniu " + imie);
						break;	
					}
					if(splitStrings[1].equals("taskFproject")) {//usun zadanie z projektu
						int nr = Integer.parseInt(splitStrings[2]);
						int nr2 = Integer.parseInt(splitStrings[3]);
						String text = projekty.get(nr).getTask(nr2).getText();
						projekty.get(nr).removeTask(nr2);
						System.out.println("Usunalem zadanie " + text + " z projektu " + projekty.get(nr).getName());
						break;	
					}
					if(splitStrings[1].equals("personFproject")) {//usun osobe z projektu
						int nr = Integer.parseInt(splitStrings[2]);
						int nr2 = Integer.parseInt(splitStrings[3]);
						String imie = projekty.get(nr).getPerson(nr2).getName();
						projekty.get(nr).removePerson(nr2);
						System.out.println("Usunalem osobe o imieniu " + imie + " z projektu " + projekty.get(nr).getName());
						break;	
					}
					if(splitStrings[1].equals("task")) {
						int nr = Integer.parseInt(splitStrings[2]);
						String text = zadania.get(nr).getText();
						zadania.remove(nr);
						System.out.println("Usunalem zadanie " + text); 
						break;	
					}
					if(splitStrings[1].equals("all")) {
						projekty.clear();
						zadania.clear();
						osoby.clear();
						System.out.println("Usunalem wszystko"); 
						break;	
					}
					if(splitStrings[1].equals("projects")) {
						projekty.clear();
						System.out.println("Usunalem projekty"); 
						break;	
					}
					if(splitStrings[1].equals("tasks")) {
						zadania.clear();
						System.out.println("Usunalem zadania"); 
						break;	
					}
					if(splitStrings[1].equals("people")) {
						osoby.clear();
						System.out.println("Usunalem ludzi"); 
						break;	
					}
					System.out.println("Bledne polecenie");
					break;
					
					
				case "list":
					if(splitStrings.length < 3) {
						if(splitStrings[1].equals("projects")) {		
							if(!projekty.isEmpty()) {
								for(Project projekt : projekty) {
									projekt.show();
								}
							}
							else {
								System.out.println("Nie ma projektow");
							}
							break;
						}
						if(splitStrings[1].equals("tasks")) {		
							if(!zadania.isEmpty()) {
								for(Task zadanie : zadania) {
									zadanie.show();
								}
							}
							else {
								System.out.println("Nie ma zadan");
							}
							break;
						}
						if(splitStrings[1].equals("people")) {		
							if(!osoby.isEmpty()) {
								for(Person osoba : osoby) {
									osoba.show();
								}
							}
							else {
								System.out.println("Nie ma ludzi");
							}
							break;
						}
					}
					else {
						if(splitStrings[2].equals("asc")) {
							Collections.sort(zadania, new CustomComparator());
							for(Task zadanie : zadania) {
								zadanie.show();
							}
							break;
						}
						
						
						if(splitStrings[2].equals("desc")) {
							Collections.sort(zadania, new CustomComparator());
							Collections.reverse(zadania);
							for(Task zadanie : zadania) {
								zadanie.show();
							}
							break;
						}
						
						if(splitStrings[2].equals("tardy")) {
							for(Task zadanie : zadania) {
								if(zadanie.getDate().compareTo(LocalDate.now()) == -1)
									zadanie.show();
							}
							break;
						}
						
					}
						
					System.out.println("Bledne polecenie");	
					break;
						
					
				case "change":
					if(splitStrings[1].equals("project")) {
						int nr = Integer.parseInt(splitStrings[2]);
						projekty.get(nr).setName(splitStrings[3]);
						System.out.println("Zmienilem nazwe projektu o id " + projekty.get(nr).getID() + " na " + projekty.get(nr).getName());
						break;
					}
					if(splitStrings[1].equals("person")) {
						int nr = Integer.parseInt(splitStrings[2]);
						osoby.get(nr).setName(splitStrings[3]);
						System.out.println("Zmienilem imie osoby o id " + osoby.get(nr).getID() + " na " + osoby.get(nr).getName());
						break;
					}
					if(splitStrings[1].equals("task")) {
						int nr = Integer.parseInt(splitStrings[2]);
						zadania.get(nr).setText(splitStrings[3]);
						System.out.println("Zmienilem nazwe zadania o id " + zadania.get(nr).getID() + " na " + zadania.get(nr).getText());
						break;
					}
					
				case "save":
					try {
				         FileOutputStream fileOut =
				         new FileOutputStream("plik.txt");
				         ObjectOutputStream out = new ObjectOutputStream(fileOut);
				         out.writeObject(projekty);
				         out.writeObject(osoby);
				         out.writeObject(zadania);
				         out.close();
				         fileOut.close();
				         System.out.println("Zapisano");
				      } catch (IOException i) {
				         i.printStackTrace();
				      }
					break;
					
				case "restore":
					try {
						FileInputStream fileIn = new FileInputStream("plik.txt");
					   	ObjectInputStream in = new ObjectInputStream(fileIn);
					   	projekty = (List<Project>) in.readObject();
					   	osoby = (List<Person>) in.readObject();
					   	zadania = (List<Task>) in.readObject();
					  	in.close();
					  	fileIn.close();
					  	System.out.println("Wczytano");
					  } catch (IOException i) {
					  	i.printStackTrace();
					 	return;
					  } catch (ClassNotFoundException c) {
					  	System.out.println("Nie znaleziono klasy");
					  	c.printStackTrace();
					  	return;
					  }
					  break;
					  
				default: 
					System.out.println("Bledne polecenie");
	            	break;
			}    
		}
		//poza petla, koniec pracy programu
		inputReader.close();
		System.out.println("Koniec pracy programu");
	}

}
