package program1;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;



public class Task extends Obiekty implements java.io.Serializable, Comparable<Task>{
//public class Task implements java.io.Serializable{
	private static final long serialVersionUID = 1;
	private static final AtomicInteger count = new AtomicInteger(0);
	private final int taskID;
	private String text;
	public LocalDate data=null;
		
   
    private Person wykonawca; // zadanie moze miec wykonawce lub nie ( i ten wykonawaca moze byc dodany pozniej
	//wykonawca ktory jest przydzielany do zadania musi byc w PROJEKCIE, ktory ma to zadanie (if)
    
	public Task() {
		taskID = count.getAndIncrement();
	}
	
	public Task(String napis) {
		taskID = count.getAndIncrement();
		text = napis;
	}
	
	public Task(String napis, String data_) {
		taskID = count.getAndIncrement();
		text = napis;
		 DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(
		            FormatStyle.MEDIUM).withLocale(Locale.GERMAN);
		data = LocalDate.parse(data_, germanFormatter);
	}
	
	public String getText(){
		return text;
	}
	
	public void setText(String napis) {
		text = napis;
	}
	
	public void show() {
		DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(
	            FormatStyle.MEDIUM).withLocale(Locale.GERMAN);
		if(data != null)
			System.out.println("Zadanie o id " + taskID + " to " + text + ". Czas zakonczenia: " + data.format(germanFormatter));
		else{
			System.out.println("Zadanie o id " + taskID + " to " + text);
		}
	}
	
	public int getID() {
		return taskID;
	}
	
	public LocalDate getDate() {
		return data;
	}

	
	@Override
	public int compareTo(Task other) {
		LocalDate otherDate = other.getDate();
        if (data.isBefore(otherDate)) {
            return -1;
        }
        else{
        	if(data.equals(otherDate)) {
        		return 1;
        	}
        	else {
        		return 0;
        	}
        }
	}
	
	@Override
	public boolean equals(Object o) 
	{
	    if (o instanceof Task) 
	    {
	    	Task c = (Task) o;
	      if (text.equals(c.text) ) //whatever here
	         return true;
	    }
	    return false;
	}

	@Override
	public String toString() {
		return "Task [taskID=" + taskID + ", text=" + text + ", data=" + data + ", wykonawca=" + wykonawca + "]";
	}
	
}
