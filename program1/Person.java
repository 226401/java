package program1;
import java.util.concurrent.atomic.AtomicInteger;

public class Person extends Obiekty  implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private static final AtomicInteger count = new AtomicInteger(0);
	private final int personID;
	private String name;
	
	public Person() {
		personID = count.getAndIncrement();
	}
	
	public Person(String imie) {
		personID = count.getAndIncrement();
		name = imie;
	}
	
	public void show() {
		System.out.println("Osoba o ID " + personID + " ma na imie " + name);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String imie) {
		name = imie;
	}
	
	public int getID() {
		return personID;
	}
	
	@Override
	public boolean equals(Object o) 
	{
	    if (o instanceof Person) 
	    {
	    	Person c = (Person) o;
	      if (name.equals(c.name) ) //whatever here
	         return true;
	    }
	    return false;
	}

	@Override
	public String toString() {
		return "Person [personID=" + personID + ", name=" + name + "]";
	}
	
	
}
